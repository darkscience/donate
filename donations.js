let donations = [
    {
        "alias": "obviyus",
        "value": 4.45,
        "currency": "USD",
        "timestamp": "2020-07-11"
    },
    {
        "alias": "garandil",
        "value": 6.87,
        "currency": "GBP",
        "timestamp": "2020-06-19"
    },
    {
        "alias": "kylef",
        "value": 3.14,
        "currency": "EUR",
        "timestamp": "2020-05-28"
    },
    {
        "alias": "garandil",
        "value": 8.54,
        "currency": "GBP",
        "timestamp": "2020-05-28"
    },
    {
        "alias": "crucio",
        "value": 0.0071,
        "currency": "BTC",
        "timestamp": "2020-05-27"
    },
    {
        "alias": "chik",
        "value": 30,
        "currency": "USD",
        "timestamp": "2020-05-20"
    },
    {
        "alias": "chromis",
        "value": 25,
        "currency": "USD",
        "timestamp": "2020-05-10"
    },
    {
        "alias": "liothen",
        "value": 100,
        "currency": "USD",
        "timestamp": "2020-05-10"
    },
    {
        "alias": "kilroy",
        "value": 18.72,
        "currency": "EUR",
        "timestamp": "2020-02-08"
    },
    {
        "alias": "WarOfDevil",
        "value": 50.00,
        "currency": "EUR",
        "timestamp": "2020-02-07"
    },
    {
        "alias": "zer0rest",
        "value": 5.00,
        "currency": "EUR",
        "timestamp": "2020-02-04"
    },
    {
        "alias": "Liothen",
        "value": 100.00,
        "currency": "USD",
        "timestamp": "2020-01-18"
    },
    {
        "alias": "garandil",
        "value": 12.34,
        "currency": "EUR",
        "timestamp": "2020-01-16"
    },
    {
        "alias": "garandil",
        "value": 12.12,
        "currency": "EUR",
        "timestamp": "2019-12-12"
    },
    {
        "alias": "Ice_Dragon",
        "value": 50.00,
        "currency": "GBP",
        "timestamp": "2019-10-11"
    },
    {
        "alias": "garandil",
        "value": 8.84,
        "currency": "EUR",
        "timestamp": "2019-08-08"
    }
];

// google 1 gbp in usd
let currencies = {
    "GBP": 1.23,
    "USD": 1,
    "EUR": 1.12,
    "BTC": 9252.9
};


/**
 * Number.prototype.format(n, x)
 *
 * @param integer n: length of decimal
 * @param integer x: length of sections
 */
Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

let total = 0;
let goal = 1000;
window.onload = function() {
    let progressbar = jQuery('.progress-bar');
    progressbar.css('width', '0%');
    jQuery.each(donations, function(idx, donation) {
        // do currency conversion
        let value = parseFloat(donation['value'], 2);
        let currency = donation['currency'];

        let exchangeRate = parseFloat(currencies[currency], 2);
        let convertedValue = value * exchangeRate;

        // add it to total
        // console.log('adding donation!', convertedValue);
        total += convertedValue;
    });

    jQuery('#raised').text(total.format(2)+' USD');
    jQuery('#backers').text(donations.length);
    jQuery('#total').text(goal.format(2)+' USD');
    let percentage = (total/goal * 100).toFixed(2);
    if (percentage > 100) {
        percentage = 100;
    }
    // console.log('percentage', percentage);
    progressbar.css('width', percentage+'%').prop('aria-valuenow', percentage);
    jQuery('.progress-bar span').html(parseInt(percentage)+'%');
};
